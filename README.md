[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
# bravo_ros

Meta-repo containing the bravo_description package and the bravo_7_ikfast plugin generated from the description.

To update the IKFast plugin for, e.g., ROBOT.xacro:

> :warning: **Warning:**: I think the resulting IKFast solution is only valid for ROBOT.xacro.

```
cd bravo_ros
rosrun xacro xacro -o ROBOT.urdf bravo_description/urdf/ROBOT.xacro
rosrun moveit_kinematics auto_create_ikfast_moveit_plugin.sh --iktype Transform6D ROBOT.urdf arm bravo_base_link ee_link
```

Remember to update bravo_moveit_config/config/kinematics.yaml

See the package `bravo_description` for tf information/frames.
