cmake_minimum_required(VERSION 3.0.2)
project(bravo_gazebo)

# # Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS gazebo_ros)

# ##################################
# # catkin specific configuration ##
# ##################################
catkin_package()

catkin_install_python(PROGRAMS scripts/unpause_simulation.py
                      DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})


# ##########
# # Build ##
# ##########
include_directories()
