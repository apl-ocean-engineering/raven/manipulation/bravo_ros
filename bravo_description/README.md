# bravo_description

URDF and mesh files associated with the APL setup of the Bravo 7 arm.
This has grown to include both Bravo URDF descriptions as well as various testing setups.


Launch file `view_bravo_7.launch` is used to visualize the urdf.

Material colors are all defined in the `/include/materials.xacro` file.

To regenerate URDF:
`rosrun xacro xacro -o ROBOT.urdf ROBOT.xacro`

## URDF Options
We include multiple test setups that are specific to our test environments.

* hh101_setup.xacro: Simulates a tabletop testing environment with Trisect
* hh104_setup.xacro: Simulates the HH101 test tank at APL

For the arm:
* bravo_arm_only.xacro: Just the arm + end effector
* bravo_hh101_calibration.xacro: arm + calibration end effector in HH101
* bravo_hh101.xacro: arm + hh101_setup
* bravo_hh101_testbed.xacro: arm + h101_setup + testbed setup

The /simulation folder includes the necessary files that have gazebo plugins.

## hh101
`roslaunch bravo_description view_bravo_7.launch test_setup:=hh101`

![screenshot](assets/hh101.png)

## hh101_testbed
`roslaunch bravo_description view_bravo_7.launch test_setup:=hh101_testbed`

![screenshot](assets/hh101_testbed.png)

## hh104
`roslaunch bravo_description view_bravo_7.launch test_setup:=hh104`

![screenshot](assets/hh104.png)

## tf tree
The resulting tf tree for the testebed setup looks like this:
![screenshot](assets/full_tf_tree.png)

## Robot EE Frame
And the robot ee looks like this:
![screenshot](assets/ee_frames.png)
