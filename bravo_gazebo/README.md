[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
# Bravo Gazebo

## About
Gazebo configuration for Bravo arm.

This repo contains world files, launch files, and configuration files for task specific simulation and the Gazebo simulated Bravo Arm controllers.

The simulator replaces the Bravo Arm setup and the stereo camera setup.

The rest of the pipeline is designed to be run as-is.

Here's a screenshot of the simulator running with the `object_detection` pipeline working on simulated images:

![screenshot](assets/rviz_screenshot.png)

## Usage

To launch, run:
`roslaunch bravo_gazebo simulation.launch`

## URDF Options
We include multiple test setups that are specific to our test environments.

* hh101_setup.xacro: Simulates a tabletop testing environment with Trisect
* hh104_setup.xacro: Simulates the HH101 test tank at APL

For the arm:
* bravo_arm_only.xacro: Just the arm + end effector
* bravo_hh101_calibration.xacro: arm + calibration end effector in HH101
* bravo_hh101.xacro: arm + hh101_setup
* bravo_hh101_testbed.xacro: arm + h101_setup + testbed setup

The /simulation folder includes the necessary files that have gazebo plugins.

## hh101
`roslaunch bravo_description simulation.launch test_setup:=hh101`

This is not a likely use-case, as if you wanted to run the partial test setup, you'd probably be replaying data (and wouldn't need the gazebo plugins for Trisect).

![screenshot](assets/hh101_simulation.png)

## hh101_testbed
`roslaunch bravo_description simulation.launch test_setup:=hh101_testbed`

![screenshot](assets/hh101_testbed_simulation.png)

## hh104
`roslaunch bravo_description simulation.launch test_setup:=hh104`

![screenshot](assets/hh104_simulation.png)

## Updating Simulation Models
To add new models/CAD to the simulator, you must define a new model inside the `models/` folder. See the `kettlebell` model as an example.

You can then spawn the model inside the world (see the `/worlds/kettlebell_world.world` file as an example).
