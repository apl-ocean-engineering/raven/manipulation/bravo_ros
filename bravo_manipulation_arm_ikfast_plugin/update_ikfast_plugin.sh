search_mode=OPTIMIZE_MAX_JOINT
srdf_filename=bravo_hh104.srdf
robot_name_in_srdf=bravo_manipulation
moveit_config_pkg=bravo_manipulation_moveit_config
robot_name=bravo_manipulation
planning_group_name=arm
ikfast_plugin_pkg=bravo_manipulation_arm_ikfast_plugin
base_link_name=bravo_base_link
eef_link_name=ee_link
ikfast_output_path=/home/mmicatka/Documents/raven_manipulation/src/bravo_ros/bravo_manipulation_arm_ikfast_plugin/src/bravo_manipulation_arm_ikfast_solver.cpp

rosrun moveit_kinematics create_ikfast_moveit_plugin.py\
  --search_mode=$search_mode\
  --srdf_filename=$srdf_filename\
  --robot_name_in_srdf=$robot_name_in_srdf\
  --moveit_config_pkg=$moveit_config_pkg\
  $robot_name\
  $planning_group_name\
  $ikfast_plugin_pkg\
  $base_link_name\
  $eef_link_name\
  $ikfast_output_path
