#! /usr/bin python3

import time

import rospy
from std_srvs.srv import Empty


def unpause_gazebo():
    # Initialize the ROS node
    rospy.init_node("unpause_simulation", anonymous=True)
    wait_time = rospy.get_param("~wait_time", 5)
    rospy.loginfo(f"Going to unpause simulation in {wait_time} seconds!")
    time.sleep(wait_time)
    rospy.loginfo("waiting for service...")
    rospy.wait_for_service("/gazebo/unpause_physics", timeout=20)

    try:
        unpause_physics = rospy.ServiceProxy("/gazebo/unpause_physics", Empty)
        # Call the service
        unpause_physics()

        rospy.loginfo("Gazebo physics unpaused successfully!")
    except rospy.ServiceException as ex:
        rospy.logerr(f"Service call failed: {ex}")


if __name__ == "__main__":
    try:
        unpause_gazebo()
    except rospy.ROSInterruptException:
        pass
